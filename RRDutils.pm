
package RRDutils;

use warnings;
use strict;
use experimental 'smartmatch';

use RRDs;

BEGIN {
   use Exporter ();
    our ( $VERSION , @ISA , @EXPORT , @EXPORT_OK , %EXPORT_TAGS );

   $VERSION="5.5";
   @ISA = qw(Exporter);   
   @EXPORT = qw(
      $confdir $datadir $graphdir $boarddir $mainGraph
      getHosts getTags %getNAME
      );
   @EXPORT_OK = qw(
      getStep getStart getDSs getRRAs
      @figSize %graphInterval @colors
      graphConfig getGraphs getBoard getLabels
      rrdCreate rrdGraph
      openWeb closeWeb htmlAnchor getNodeLabels
      %getDEFs %getNAME
      );
   %EXPORT_TAGS = (
      Create => [ qw(getStep getStart getDSs getRRAs rrdCreate) ]
    , Graph => [ qw(
        @figSize %graphInterval @colors
        graphConfig getGraphs getLabels rrdGraph
        %getDEFs %getNAME
        ) ]
    , Web => [ qw(graphConfig getGraphs getBoard
        openWeb closeWeb htmlAnchor getNodeLabels) ]
      );
   }

sub mainConfig($);

my $mainconf = "/etc/rrdUtils.conf";

our ( $confdir , $datadir , $graphdir , $boarddir , $mainGraph );
our ( %getDEFs , %getNAME );

my $rootdir = "RRDUTILS_ROOT";
$rootdir = $ENV{"ProgramFiles"}."\\rrdUtils" if $ENV{"ProgramFiles"};

mainConfig($mainconf);

my $defStep = 300;
my @defGraph = ( 300, 90 );

our ( @figSize , %graphInterval , @colors );

@figSize = ( "--width" , "$defGraph[0]" , "--height" , "$defGraph[1]" );

%graphInterval = (
   'hourly' => -7200 ,
   'daily' => -86400 ,
   'weekly' => -604800 ,
   'monthly' => -2592000 ,
   'quarterly' => -10368000 ,
   'yearly' => -41472000 );

@colors= qw(ff0000 00ff00 0000ff aa0000 00aa00 0000aa
   880000 008800 000088 660000 006600 000066
   ff0000 00ff00 0000ff ff0000 00ff00 0000ff
   ff0000 00ff00 0000ff ff0000 00ff00 0000ff
   ff0000 00ff00 0000ff ff0000 00ff00 0000ff
   ff0000 00ff00 0000ff ff0000 00ff00 0000ff
   ff0000 00ff00 0000ff ff0000 00ff00 0000ff
   ff0000 00ff00 0000ff ff0000 00ff00 0000ff);


sub mainConfig($) {
   my $conf = shift;
   my ( $data , $graph , $board );
   if ( open CFG , "$conf" ) {
      while (<CFG>) {
         next if /^\s*#/; s/\s*$//;
         if ( /^RootDir\s+(.+)$/ ) {
            $rootdir=$1;
            }
         if ( /^DataDir\s+(.+)$/ ) {
            $data=$1;
            }
         if ( /^GraphDir\s(.+)$/ ) {
            $graph=$1;
            }
         if ( /^BoardDir\s(.+)$/ ) {
            $board=$1;
            }
         }
      close CFG;
      }
   $confdir = "$rootdir/conf";
   $datadir = defined($data) ? $data : "$rootdir/data";
   $graphdir = defined($graph) ? $graph : "$rootdir/graphs";
   $boarddir = defined($board) ? $board : "$rootdir/boards";
   }

sub graphConfig($) {
   my $conf = shift;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( /^DefGraph(\s+([a-z_]+))?(\s+([0-9]+)x([0-9]+))?$/ ) {
         $mainGraph = $2 if $2;
         if ( $3 ) {
            @defGraph = ( $4 , $5 );
            @figSize = ( "--width" , "$defGraph[0]" , "--height" , "$defGraph[1]" );
            }
         }
      if ( /^I\s+([^\s]+)\s+(\d+)/ ) {
         $graphInterval{$1} = "-$2";
         }
      }
   close CFG;
   }

sub getStep($) {
   my $conf = shift;
   my $step;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( /^BaseStep\s+([0-9]+)/ ) {
         warn "WARNING : base step redefined\n" if $step;
         $step=$1;
         }
      }
   close CFG;
   $step = $defStep unless $step;
   return $step;
   }

sub getStart($) {
   my $conf = shift;
   my $start;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( /^Start\s+(.*)/ ) {
         warn "WARNING : Start time for RRDB defined twice" if $start;
         $start=$1;
         }
      }
   return $start;
   }

sub getHosts($) {
   my ( $conf , $tag ) = split /\./ , shift , 2 ;
   $tag = "" unless defined($tag);
   my @hosts;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( s/^H$tag\s+// ) {
         push @hosts , split;
         }
      if ( s/^T\s+// ) {
         if ( $tag ) {
            @_ = split;
            push @hosts , [ $_[0] , $tag ] if ( $tag ~~ @_[1..$#_] );
         } else {
            push @hosts , [ split ];
            }
         }
      }
   close CFG;
   return @hosts;
   }

sub getTags($) {
   my $conf = shift;
   my %tags;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( s/^H([^\s]+)\s// ) {
         $tags{$1} = $1;
         }
      if ( s/^T\s+// ) {
         @_ = split;
         map { $tags{$_} = $_ } @_[1..$#_];
         }
      }
   close CFG;
   return keys %tags;
   }


sub getDSs($) {
   my $conf = shift;
   my %types = (
      'G' => 'GAUGE' ,
      'C' => 'COUNTER' ,
      'D' => 'DERIVE' ,
      'A' => 'ABSOLUTE' );
   my %DSs = ( '' => [] );
   my $step = getStep $conf;
   return unless open CFG , "$confdir/$conf";
   $_ = <CFG>; s/\s*$//;
   while ( 1 ) {
      last if eof CFG;
      if ( /^[^#]/ && /^([^\s\[]+)?\[([^\s\/]+)\](\s+(.*))?$/ ) {
         my $label = $2;
         my ( $type, $beat, $min, $max ) = ( "COUNTER", 3 , "U", "U" );
         $_ = <CFG>; $_ =~ s/\s*$//;
         if ( s/^((C|G|D|A)\s?)?\s*([1-9\.]+)?(\s+((U|-?[0-9\.]+)\s+)?(U|-?[0-9\.]+))?$// ) {
            $type = $types{$2} if defined($2);
            $beat = $3 if defined($3);
            $min = $6 if defined($6);
            $max = $7 if defined($7);
            unless ( eof CFG ) { $_ = <CFG> ; s/\s*$//; }
            }
         my @items = split /\./ , $label , 2;
         if ( scalar(@items) > 1 ) {
            defined($DSs{$items[0]}) or $DSs{$items[0]} = [];
            push @{$DSs{$items[0]}} , "DS:$items[1]:$type:" . $beat * $step . ":$min:$max";
         } else {
            push @{$DSs{''}} , "DS:$label:$type:" . $beat * $step . ":$min:$max";
            }
      } else {
         last if eof CFG;
         $_ = <CFG>; s/\s*$//;
         }
      }
   close CFG;

   for ( keys %DSs ) {
      if ( scalar(@{$DSs{$_}}) == 0 ) {
         delete $DSs{$_};
         }
      }

   return %DSs;
   }

sub getRRAs($) {
   my $conf = shift;
   my @RRAs;
   my $TimeStep = 1;
   return unless open CFG , "$confdir/$conf";
   while ( <CFG> ) {
      next if /^\s*#/; s/\s*$//;
      if ( /^TimeStep\s+(.*)/ ) {
         $TimeStep *= $1;
         next;
         }
      if ( s/^(CF\s+)?(AVERAGE|MAX|MIN|LAST)// ) { #(.*)(\s+([0-9]+))?$/ ) {
         my $CF = $2;
         my ( $xff , $step , $rows ) = ( 0.5 , 1 , 192 );
         /(\s+(0|0?\.[0-9]+)?\s*([1-9][0-9]*)?)?\s+([0-9]+)$/;
         $xff = $2 if $2;
         $step = $3 if $3;
         $rows = $4 if $4;
         push @RRAs , "RRA:$CF:$xff:" . $step * $TimeStep . ":$rows";
         next;
         }
      }
   close CFG;
   @RRAs = ( "RRA:AVERAGE:0.5:1:2048" ) unless @RRAs;
   return @RRAs;
   }


sub getSize($) {
   my $size = shift;
   $size =~ /^(.*)x(.*)/;
   my $xsize = $1 || $defGraph[0];
   my $ysize = $2 || $defGraph[1];
   return "--width" , "$xsize" , "--height" , "$ysize";
   }

sub getGraphs($) {
   my $conf = shift;
   my $opts = "";
   my %graphs;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( s/^O(=?)\s+// ) {
         chomp;
         $opts = "$_" if $1;
         $opts .= " $_";
         }
      if ( s/^((P|C)(S|H|T)?)(_?([a-z_]+))?(\s+([0-9]*x[0-9]*))?\s+(STD|[a-z,]+)\s+// ) {
         my @vars=split;
         my @size = ();
         @size=getSize($7) if $7;
         my $idx = join "." , @vars;
         $idx = join "." , @vars[1..$#vars] if substr($1,1,1) eq "H" || substr($1,1,1) eq "T";
         $idx = $5 if $5;
         $graphs{$idx}{'type'} = $1;
         $graphs{$idx}{'size'} = \@size;
         my $period = $8;
         $period = "daily,hourly,weekly,monthly,quarterly,yearly" if $period eq 'STD' ;
         if ( $graphs{$idx}{'periods'} ) {
            $graphs{$idx}{'periods'} .= "," . $period;
         } else {
            $graphs{$idx}{'periods'} = $period;
            }
         $graphs{$idx}{'opts'} = $opts;
         $graphs{$idx}{'ds'} = \@vars;
         }
      }
   close CFG;
   return %graphs;
   }

sub getBoard($) {
   my $conf = shift;
   my %board = ( 'ncols'=>4 , 'period'=>pop [ keys %graphInterval ] , 'graphlist'=>[] , 'hosts'=>[]);
   my @graphlist;
   return unless open CFG , "$boarddir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      next unless $_;
      if ( /^Hosts\s+(.*)/ ) {
         push $board{'hosts'} , split(' ', $1);
         next;
         }
      if ( /^Period\s+(.*)/ ) {
         $board{'period'} = $1;
         next;
         }
      if ( /^Ncolumns\s+(.*)/ ) {
         $board{'ncols'} = $1;
         next;
         }

      my ( $target , @graphs ) = split;

      $target =~ s+^([^.]*)\.++ ;
      $target = "$1-$target" if $1;

      for ( @graphs ) {
         push $board{'graphlist'} , "$target $_";
         }
      }
   close CFG;
   $board{'ncols'} = 1+$#{$board{'hosts'}} if $#{$board{'hosts'}} > 1 ;
   return %board;
   }

sub getNodeLabels() {
   my %labels;
   return unless open CFG , "$rootdir/labels";
   while (<CFG>) {
      s/^\s*//;
      next if /^#/ or not $_;
      next unless /^[a-z0-9]/;
      my @v = split ' ' , $_ , 2;
      $labels{$v[0]} = $v[1];
      }
   close CFG;
   return %labels;
   }


sub getLabels($) {
   my $conf = shift;
   my %labels;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( /^([^\s\[]*)?\[(([^\s\/]+)(\.value)?)\](\s+(.*))?$/ ) {
         my $key = $3;
         my $label = $6 || $1 || $2;
         $key =~ s/\.value$//;
         $label =~ s/\.value$//;
         $labels{$key} = $label;
         $labels{$key} =~ s/\s*$//;
         }
      }
   close CFG;
   return %labels;
   }

sub rrdCreate {
   my ( $host , $target , @args ) = @_ ;
   if ( ref($host) eq "ARRAY" ) {
      my $node = shift @$host;
      my @res = map { rrdCreate( $node , "$target.$_" , @args ) } @$host;
      return join(", ", grep { defined($_) } @res);
      }
   my $rrdname = $getNAME{'rrd'}( $host , $target );
   if ( -e "$rrdname" ) {
      return "$target for $host exists, not overwritten";
      }
   my @target = split '/' , $target;
   mkdir "$datadir/$host";
   mkdir "$datadir/$host/$target[0]" if scalar(@target)>1;
   mkdir "$graphdir/$host";
   mkdir "$graphdir/$host/$target[0]";
   RRDs::create ( "$rrdname" , @args );
   if ( $< == 0 ) {
      chown int(getpwnam("rrd")),int(getgrnam("rrd")) , $rrdname;
      }
   chmod 0774 , $rrdname;
   return RRDs::error;
   }

# To use also with PH plots, the title and other items differ
sub rrdGraph {
   my ( $host , $target , $graph , $period , @graphargs ) = @_ ;
   my $imgname = $getNAME{'img'}( $host , $target , $graph , $period );
   $host =~ s/\..*$//;
   my $title = "$target @ $host";
   $title = "$host - $graph" unless $target;
   RRDs::graph ( $imgname , "--title=$title" , @graphargs );
   return RRDs::error;
   }

my $style = "bgcolor=#ffffff text=#000000" ;
my $tabstyle = "BORDER=0 CELLPADDING=0 CELLSPACING=10" ;

sub openWeb($$;$) {
   my $file = shift;
   my $title = shift;
   my $ttl = 3600;
   if ( @_ ) {
      $ttl = shift;
      $ttl = int( 0.05 * abs($graphInterval{$ttl}) )
      };
   open my $html , '>' , "$graphdir/$file.html" ;
   print $html "<HTML>\n<HEAD>\n";
   print $html "<META HTTP-EQUIV=Refresh CONTENT=$ttl>\n";
   print $html "<TITLE>$title</TITLE>\n";
   print $html "</HEAD>\n<BODY $style>\n";
   print $html "<TABLE $tabstyle>\n";
   return $html;
   }

sub closeWeb($) {
   my $html = shift ;
   print $html "</TABLE>\n";
   print $html "</BODY>\n</HTML>\n";
   close $html;
   }

sub htmlAnchor($@) {
   my $src = shift ;
   # Thist should be a call to getNAME{'html'}
   my $html = join "/" , @_ ;
   return "<td>" .
      ( $html ? "<A HREF=$html.html>" : "" ) .
      "<IMG SRC=$src>" .
      ( $html ? "</A>" : "" ) .
      "</td>\n";
   }

my %CFs = ( 'a' => 'AVERAGE' , 'm' => 'MIN' , 'M' => 'MAX' , 'l' => 'LAST' );

sub cfType($) {
   my @cdef = split /,/ , shift;
   my $type = 'AVERAGE';
   my $dsname = shift @cdef;
#WinXP   return ( $dsname , $type , @cdef );
   $dsname =~ s/^((a|m|M|l)_)?//;
   $type = $CFs{$2} if defined($2);
   return ( $dsname , $type , @cdef );
   }

$getDEFs{'PT'} = sub {
   my ( $dsname , $host , $legend , @tags ) = @_;
   my ( $cf , $rrdname , @cmd , @cdef );
   ( $dsname , $cf , @cdef ) = cfType $dsname;
   for my $n ( 0 .. $#tags ) {
      $rrdname = $getNAME{'rrd'}( $host , $tags[$n] );
      $rrdname =~ s/:/\\:/;
      if ( @cdef ) {
         push @cmd , "DEF:ds$n=$rrdname:$dsname:$cf";
         push @cmd , "CDEF:d$n=ds$n," . join(',',@cdef);
      } else {
         push @cmd , "DEF:d$n=$rrdname:$dsname:$cf";
         }
      push @cmd , "LINE1:d$n#$colors[$n]:$tags[$n]" . ($cf eq "MIN"?" MIN":"") . ($cf eq "MAX"?" MAX":"");
      }
   return @cmd;
   };
$getDEFs{'PH'} = sub {
   my ( $dsname , $target , $legend , @hosts ) = @_;
   my ( $cf , $rrdname , @cmd , @cdef );
   ( $dsname , $cf , @cdef ) = cfType $dsname;
   for my $n ( 0 .. $#hosts ) {
      if ( ref($hosts[$n]) eq "ARRAY" ) {
         $hosts[$n] = $hosts[$n]->[0];
         }
      if ( $hosts[$n] =~ s/:(.*)$// ) {
         $rrdname = $getNAME{'rrd'}( $hosts[$n] , $1 );
      } else {
         $rrdname = $getNAME{'rrd'}( $hosts[$n] , $target );
         }
      $rrdname =~ s/:/\\:/;
      if ( @cdef ) {
         push @cmd , "DEF:ds$n=$rrdname:$dsname:$cf";
         push @cmd , "CDEF:d$n=ds$n," . join(',',@cdef);
      } else {
         push @cmd , "DEF:d$n=$rrdname:$dsname:$cf";
         }
      push @cmd , "LINE1:d$n#$colors[$n]:$hosts[$n]" . ($cf eq "MIN"?" MIN":"") . ($cf eq "MAX"?" MAX":"");
      }
   return @cmd;
   };
$getDEFs{'PS'} = sub {
   my ( $host , $target , $legend , @dsnames ) = @_;
   my ( $dsname , $cf , $label , $rrdname , @cmd , @cdef );
   ( $dsname , $cf , @cdef ) = cfType $dsnames[0];
   $label  = $#cdef+1 ? "$dsname," . join(',',@cdef) : $$legend{$dsname};
   $label .= ($cf eq "MIN"?" MIN":"") . ($cf eq "MAX"?" MAX":"");
   $rrdname = $getNAME{'rrd'}( $host , $target );
   if ( @cdef ) {
      push @cmd , "DEF:ds0=$rrdname:$dsname:$cf";
      push @cmd , "CDEF:d0=ds0," . join(',',@cdef);
   } else {
      push @cmd , "DEF:d0=$rrdname:$dsname:$cf";
      }
   push @cmd , "AREA:d0#$colors[0]:$label";
   for my $n ( 1 .. $#dsnames ) {
      ( $dsname , $cf , @cdef ) = cfType $dsnames[$n];
      $label  = $#cdef+1 ? "$dsname," . join(',',@cdef) : $$legend{$dsname};
      $label .= ($cf eq "MIN"?" MIN":"") . ($cf eq "MAX"?" MAX":"");
      $rrdname = $getNAME{'rrd'}( $host , $target );
      $rrdname =~ s/:/\\:/;
      if ( @cdef ) {
         push @cmd , "DEF:ds$n=$rrdname:$dsname:$cf";
         push @cmd , "CDEF:d$n=ds$n," . join(',',@cdef);
      } else {
         push @cmd , "DEF:d$n=$rrdname:$dsname$cf";
         }
      push @cmd , "STACK:d$n#$colors[$n]:$label";
      }
   return @cmd;
   };
$getDEFs{'P'} = sub {
   my ( $host , $target , $legend , @dsnames ) = @_;
   my ( $dsname , $cf , $label , $rrdname , @cmd , @cdef );
   for my $n ( 0 .. $#dsnames ) {
      ( $dsname , $cf , @cdef ) = cfType $dsnames[$n];
      $label  = $#cdef+1 ? "$dsname," . join(',',@cdef) : $$legend{$dsname};
      $label .= ($cf eq "MIN"?" MIN":"") . ($cf eq "MAX"?" MAX":"");
      $rrdname = $getNAME{'rrd'}( $host , $target );
      $rrdname =~ s/:/\\:/;
      if ( @cdef ) {
         push @cmd , "DEF:ds$n=$rrdname:$dsname:$cf";
         push @cmd , "CDEF:d$n=ds$n," . join(',',@cdef);
      } else {
         push @cmd , "DEF:d$n=$rrdname:$dsname:$cf";
         }
      push @cmd , "LINE1:d$n#$colors[$n]:$label";
      }
   return @cmd;
   };
$getDEFs{'CT'} = sub {
   my ( $dsname , $host , $legend , @tags ) = @_;
   my ( $cf , $rrdname , @cmd , @cdef );
   my ( $metric , $target , $idx );
   ( $target , $dsname ) = split /\./ , $dsname , 2;;
   ( $target , $idx ) = split /\./ , $target , 2;
   ( $dsname , $cf , @cdef ) = cfType $dsname;
   ( $metric , $dsname ) = split /\./ , $dsname , 2;
   $metric .= "-$idx" if $idx;
   $dsname = "value" unless ( defined($dsname) );
   for my $n ( 0 .. $#tags ) {
      if ( substr($target, -1) eq '-' ) {
         $rrdname = $getNAME{'rrd'}( $host, "$target$tags[$n]/$metric" );
      } else {
         $rrdname = $getNAME{'rrd'}( $host, "$target/$metric-$tags[$n]" );
         }
      $rrdname =~ s/:/\\:/;
      if ( @cdef ) {
         push @cmd , "DEF:ds$n=$rrdname:$dsname:$cf";
         push @cmd , "CDEF:d$n=ds$n," . join(',',@cdef);
      } else {
         push @cmd , "DEF:d$n=$rrdname:$dsname:$cf";
         }
      push @cmd , "LINE1:d$n#$colors[$n]:$tags[$n]" . ($cf eq "MIN"?" MIN":"") . ($cf eq "MAX"?" MAX":"");
      }
   return @cmd;
   };
$getDEFs{'CH'} = sub {
   my ( $dsname , $target , $legend , @hosts ) = @_;
   my ( $cf , $metric , $rrdname , @cmd , @cdef );
   ( $dsname , $cf , @cdef ) = cfType $dsname;
   ( $metric , $dsname ) = split /\./ , $dsname , 2;
   $dsname = "value" unless ( defined($dsname) );
   for my $n ( 0 .. $#hosts ) {
      if ( ref($hosts[$n]) eq "ARRAY" ) {
         $hosts[$n] = $hosts[$n]->[0];
         }
      if ( $hosts[$n] =~ s/:(.*)$// ) {
         $rrdname = $getNAME{'rrd'}( $hosts[$n] , "$target-$1/$metric" );
      } else {
         $rrdname = $getNAME{'rrd'}( $hosts[$n] , "$target/$metric" );
      }
      $rrdname =~ s/:/\\:/;
      if ( @cdef ) {
         push @cmd , "DEF:ds$n=$rrdname:$dsname:$cf";
         push @cmd , "CDEF:d$n=ds$n," . join(',',@cdef);
      } else {
         push @cmd , "DEF:d$n=$rrdname:$dsname:$cf";
         }
      push @cmd , "LINE1:d$n#$colors[$n]:$hosts[$n]" . ($cf eq "MIN"?" MIN":"") . ($cf eq "MAX"?" MAX":"");
      }
   return @cmd;
   };
$getDEFs{'CS'} = sub {
   my ( $host , $target , $legend , @dsnames ) = @_;
   my ( $dsname , $cf , $label , $rrdname , @cmd , @cdef );
   my ( $metric , $idx );
   ( $target , $idx ) = split /\./ , $target , 2;
   ( $dsname , $cf , @cdef ) = cfType $dsnames[0];
   $label  = $#cdef+1 ? "$dsname," . join(',',@cdef) : $$legend{$dsname};
   $label .= ($cf eq "MIN"?" MIN":"") . ($cf eq "MAX"?" MAX":"");
   ( $metric , $dsname ) = split /\./ , $dsname , 2;
   $metric .= "-$idx" if $idx;
   $dsname = "value" unless ( defined($dsname) );
   $rrdname = $getNAME{'rrd'}( $host , "$target/$metric" );
   if ( @cdef ) {
      push @cmd , "DEF:ds0=$rrdname:$dsname:$cf";
      push @cmd , "CDEF:d0=ds0," . join(',',@cdef);
   } else {
      push @cmd , "DEF:d0=$rrdname:$dsname:$cf";
      }
   push @cmd , "AREA:d0#$colors[0]:$label";
   for my $n ( 1 .. $#dsnames ) {
      ( $dsname , $cf , @cdef ) = cfType $dsnames[$n];
      $label  = $#cdef+1 ? "$dsname," . join(',',@cdef) : $$legend{$dsname};
      $label .= ($cf eq "MIN"?" MIN":"") . ($cf eq "MAX"?" MAX":"");
      ( $metric , $dsname ) = split /\./ , $dsname , 2;
      $metric .= "-$idx" if $idx;
      $dsname = "value" unless ( defined($dsname) );
      $rrdname = $getNAME{'rrd'}( $host , "$target/$metric" );
      $rrdname =~ s/:/\\:/;
      if ( @cdef ) {
         push @cmd , "DEF:ds$n=$rrdname:$dsname:$cf";
         push @cmd , "CDEF:d$n=ds$n," . join(',',@cdef);
      } else {
         push @cmd , "DEF:d$n=$rrdname:$dsname:$cf";
         }
      push @cmd , "STACK:d$n#$colors[$n]:$label";
      }
   return @cmd;
   };
$getDEFs{'C'} = sub {
   my ( $host , $target , $legend , @dsnames ) = @_;
   my ( $dsname , $cf , $label , $rrdname , @cmd , @cdef );
   my ( $metric , $idx );
   ( $target , $idx ) = split /\./ , $target , 2;
   for my $n ( 0 .. $#dsnames ) {
      ( $dsname , $cf , @cdef ) = cfType $dsnames[$n];
      $label  = $#cdef+1 ? "$dsname," . join(',',@cdef) : $$legend{$dsname};
      $label .= ($cf eq "MIN"?" MIN":"") . ($cf eq "MAX"?" MAX":"");
      ( $metric , $dsname ) = split /\./ , $dsname , 2;
      $metric .= "-$idx" if $idx;
      $dsname = "value" unless defined($dsname) ;
      $rrdname = $getNAME{'rrd'}( $host , "$target/$metric" );
      $rrdname =~ s/:/\\:/;
      if ( @cdef ) {
         push @cmd , "DEF:ds$n=$rrdname:$dsname:$cf";
         push @cmd , "CDEF:d$n=ds$n," . join(',',@cdef);
      } else {
         push @cmd , "DEF:d$n=$rrdname:$dsname:$cf";
         }
      push @cmd , "LINE1:d$n#$colors[$n]:$label";
      }
   return @cmd;
   };

# FIXME: no index yet
# IMPROVE: The easiest way is just use a single char as separator, betwen
#          host & target , $graph & period , ....
$getNAME{'rrd'} = sub {
   my ( $host , $target ) = @_ ;
   # Check for collectd file
   if ( -e "$datadir/$host-$target.rrd" ) {
      return "$datadir/$host-$target.rrd";
      }
   return "$datadir/$host/$target.rrd";
   };
$getNAME{'img'} = sub {
   my ( $host , $target , $graph , $period ) = @_ ;
   return "$graphdir/$host/$target/$graph.$period.png";
   };
$getNAME{'src'} = sub {
   my $period = pop ;
   return join( "/" , @_ ) . ".$period.png";
   };
sub htmName($$;$) {
   my $host = shift;
   my $target = shift;
   if ( @_ ) { $target .= "/" . shift }
   if ( $host eq "-" ) { return $target }
   return "$graphdir/$host/$target.html";
   }

1;
