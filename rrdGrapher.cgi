#!/usr/bin/perl

use CGI::Lite;
use RRDutils qw( :DEFAULT :Graph );

use warnings;
use strict;

# IMPROVE: On error, return also an image, with informational text
# IMPROVE: Include refresh parameter on HTTP header, based on period

my $cgi = new CGI::Lite;
my %form = $cgi->parse_form_data();
if ( $cgi->is_error() ) {
   close_conn $cgi->get_error_message();
   }

sub close_conn {
   print "Content-Type: text/html\n\n";
   $cgi->return_error( @_ );
   exit 1;
   }

sub handler {
   my ( $target , $host , $graph , $period ) = @_;

   close_conn "Insuficient data : no configuration selected" unless $target;
   close_conn "Insuficient data : no host specified" unless $host;
   close_conn "Insuficient data : no graph selected" unless $graph;
   close_conn "Insuficient data : no period supplied" unless $period;

   my ( $conf , $tag ) = split /\./ , $target , 2 ;
   close_conn "No '$conf' configuration file found" unless -f "$confdir/$conf" ;

   close_conn "Unknown host $host" unless -d "$datadir/$host";

   $target =~ s+^.*\.++;
   my $rrdname = $getNAME{'rrd'}( $host , $target );
   close_conn "No data to process" unless -d $rrdname ;

   graphDefaults $conf;

   my %graphs = getGraphs $conf;
   close_conn "Unknown graph $graph" unless $graphs{$graph};

   my %legend = getLabels $conf;

   my @graphSize = @{$graphs{$graph}{'size'}};
   @graphSize = @figSize unless @graphSize;

# FIXME: In this way, custom timespans are not allowed. To allow custom
#        timespans, graphInterval should be modified, and caching mode should
#        also be disabled
   my %periods = map { $_ => $_ } split /,/ , $graphs{$graph}{'periods'} ;
   close_conn "Unknown '$period' time span specified" unless $periods{$period};
# Lo que no entiendo es porque digo que hay que deshabilitar la cache ....

   my @cmdargs = ( @graphSize , "--lazy" , "--start" , "$graphInterval{$period}" );
   push @cmdargs , "$graphs{$graph}{'opts'}" if $graphs{$graph}{'opts'};

   my $type = $graphs{$graph}{'type'};
   my @vars = @{$graphs{$graph}{'ds'}};

   my @cmd = $getDEFs{$type}($host,$target,\%legend,@vars);

   print "Content-Type: image/gif\n\n";
# FIXME: Write to an actual file to allow caching
   $host =~ s/\..*$//;
   RRDs::graph ( "-" , @cmdargs , "--title=$target @ $host" , @cmd );
# That means to use rrdGraph ???
   my $error = RRDs::error;
   close_conn "ERROR : $error" if $error ;
   }

my ( $target , $host ) = ( $form{'target'} , $form{'host'} );
my ( $graph , $period ) = ( $form{'graph'} , $form{'period'} );

handler( $target , $host , $graph , $period );

