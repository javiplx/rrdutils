#!/usr/bin/python

from distutils.core import setup

setup(
	name = "rrdutils" ,
	version = "5.5",
	author = "Javier Palacios" ,
	author_email = "javiplx@gmail.com" ,
	py_modules = [ "rrdutils" ]
	)

