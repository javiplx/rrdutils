%{!?dist:%define dist %nil}

Summary: Utilities to deal with RRD files and graphs
Name: rrdUtils
Version: 5.5
Release: 1%{dist}
Source: RRDutils-%{version}.tar.gz
License: El Menda
Group: Applications

Requires: perl, rrdtool-perl , rrdtool-python

BuildArch: noarch
BuildRoot: %{_tmppath}/%{name}-root

%description
This is a set of tools intended to help creation and displaying of RRD
files. The RRD characteristics (data sources, consolidation functions)
and the graphs are described in a configuration file that we can use to
create new instances of a RRD, and to generate the graphs for them.


%define rrddir /var/lib/rrd

%define python_site %( %{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()" )

%prep
%setup -q -n RRDutils-%{version}

perl Makefile.PL ROOTDIR=%{rrddir}

%build

make
python setup.py build

%install

make install PERL_INSTALL_ROOT=%{buildroot}
python setup.py install --root %{buildroot}

mkdir -p %{buildroot}%{rrddir}/{conf,data,graphs,boards}

%ifos solaris2.8
%define pkgname rrdUtils
%ultimatepkg
%pkgkreator
%endif


%clean
rm -rf %{buildroot}


%pre

if ! grep -q rrd: /etc/group ; then
  groupadd -g 666 rrd
  fi

if ! grep -q rrd: /etc/passwd ; then
  useradd -u 666 -g rrd -M -d %{rrddir} -s /sbin/nologin rrd
  fi


%files
%defattr(0644,root,root,0755)
%doc README DESCRIPTION INSTALL MANIFEST
%doc ToDo ChangeLog
%doc examples contrib
%doc snmp-kit

%attr(0755,root,root) %{_bindir}/RRDcreator
%attr(0755,root,root) %{_bindir}/RRDgrapher
%attr(0755,root,root) %{_bindir}/RRDweb
%attr(0755,root,root) %{_bindir}/RRDnode
%attr(0755,root,root) %{_bindir}/RRDindex
%{perl_sitelib}/RRDutils.pm
%{perl_sitearch}/auto/RRDutils

%if "%{dist}" == ".el6"
%{python_site}/rrdutils-%{version}-py%{python_version}.egg-info
%endif
%{python_site}/rrdutils.py
%{python_site}/rrdutils.pyc
%{python_site}/rrdutils.pyo


%dir %attr(0775,rrd,rrd) %{rrddir}
%dir %attr(0775,rrd,rrd) %{rrddir}/conf
%dir %attr(0775,rrd,rrd) %{rrddir}/data
%dir %attr(0775,rrd,rrd) %{rrddir}/graphs


%changelog
* Fri Dec  7 2012 Javier Palacios <javiplx@gmail.com>  - 5.5
- Add specific user for rrdUtils files
- Added initial version of python module
- Add default graphs to examples

* Mon Aug 17 2009 Javier Palacios <javiplx@gmail.com>
- Added multi-tag graphs
- Fixed %nil

* Tue May  6 2008 Javier Palacios <javiplx@gmail.com>
- Recovered SNMP utilities
- Added RRDindex

* Sun Jan 15 2006 Javier Palacios <javiplx@gmail.com>
- Major release of 5.0 version

* Tue Nov  1 2005 Javier Palacios <javier.pb@hotmail.com>
- Modified to use the new Makefile.PL

* Sat Jun 25 2005 Javier Palacios <javier.pb@hotmail.com>
- Upgraded to rrdUtils version 4 with snmp-kit

* Sat Dec 21 2002 Javier Palacios <javier.palacios@rediris.es>
- Source code modified to use RRDs.pm

* Wed Dec 11 2002 Javier Palacios <javier.palacios@rediris.es>
- Added a configure to distribution, to ease installation pathing. Some
  problems arise with perl library path at %files section

* Thu Dec  5 2002 Javier Palacios <javier.palacios@rediris.es>
- 2.3 version. The makefile patch is deleted, with changes hardcoded
  on the source tree

* Tue Oct 29 2002 Javier Palacios <javier.palacios@rediris.es>
- Upgraded to 2.2 version

* Thu Oct 23 2002 Javier Palacios <javier.palacios@rediris.es>
- First rpm version

