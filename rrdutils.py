
import rrdtool

import os
import re

rootdir = "/var/lib/rrd"
# $rootdir = $ENV{"ProgramFiles"}."\\rrdUtils" if $ENV{"ProgramFiles"};

confdir = os.path.join(rootdir,"conf")
datadir = os.path.join(rootdir,"data")
graphdir = os.path.join(rootdir,"graphs")

defStep = 300
defGraph = [ 300, 90 ]

figSize = [ "--width" , defGraph[0] , "--height" , defGraph[1] ]

graphInterval = {
   'daily' : -86400 ,
   'weekly' : -604800 ,
   'monthly' : -2592000 ,
   'quarterly' : -10368000 ,
   'yearly' : -41472000 }

colors = [ 'ff0000', '00ff00', '0000ff', 'aa0000', '00aa00', '0000aa', 
   '880000', '008800', '000088', '660000', '006600', '000066',
   'ff0000', '00ff00', '0000ff', 'ff0000', '00ff00', '0000ff',
   'ff0000', '00ff00', '0000ff', 'ff0000', '00ff00', '0000ff',
   'ff0000', '00ff00', '0000ff', 'ff0000', '00ff00', '0000ff',
   'ff0000', '00ff00', '0000ff', 'ff0000', '00ff00', '0000ff',
   'ff0000', '00ff00', '0000ff', 'ff0000', '00ff00', '0000ff',
   'ff0000', '00ff00', '0000ff', 'ff0000', '00ff00', '0000ff' ]


def getFileContents ( conf ) :
   if conf.find(".") != -1 :
      conf , tag = conf.split(".",1)
   else :
      tag = ""
   file = os.path.join( confdir , conf )
   if not os.path.exists( file ) :
      return False , None
   lines = []
   fobj = open( file )
   for line in fobj.readlines() :
      if re.match("\s*#",line) : continue
      line.strip()
      if not line : continue
      lines.append( line )
   fobj.close()
   return tag , lines


"""
sub changeDefs($) {
   my $conf = shift;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( /^DataDir\s+(.+)$/ ) {
         $datadir=$1;
         }
      if ( /^GraphDir\s(.+)$/ ) {
         $graphdir=$1;
         }
      if ( /^DefGraph\s+([0-9]+)\s+([0-9]+)$/ ) {
         @defGraph = ( $1 , $2 );
         @figSize = ( "--width" , "$defGraph[0]" , "--height" , "$defGraph[1]" );
         }
      }
   close CFG;
   }
"""

def getStep ( conf , filelines=None ) :
   step = None
   if not filelines :
      tagname , filelines = getFileContents( conf )
      if tagname is False :
         return step
   for line in filelines :
      match = re.match("BaseStep\s+([0-9]+)",line)
      if match :
         if step :
            print "WARNING : base step redefined"
         step = int(match.groups()[0])
   if not step :
      step = defStep
   return step

"""
sub getStart($) {
   my $conf = shift;
   my $start;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( /^Start\s+(.*)/ ) {
         warn "WARNING : Start time for RRDB defined twice" if $start;
         $start=$1;
         }
      }
   return $start;
   }
"""

def getHosts ( conf ) :
   hosts = []
   tag , filelines = getFileContents( conf )
   for line in filelines :
      match = re.match("H%s\s+"%tag,line)
      if match :
         line = line[match.end():]
         hosts.extend( line.split() )
      match = re.match("T\s+",line)
      if match :
         line = line[match.end():]
         hosts.append( line.split() )
   return hosts


def getTags ( conf ) :
   tags = {}
   tagname , filelines = getFileContents( conf )
   for line in filelines :
      match = re.match("H([^\s]+)\s",line)
      if match :
         tags[ match.groups()[0] ] = True
   return tags.keys()



def getDSs ( conf ) :
   DSs = []
   tagname , lines = getFileContents( conf )
   if tagname is False :
      return []
   step = getStep( conf , lines )
   types = {
      'G' : 'GAUGE' ,
      'C' : 'COUNTER' ,
      'D' : 'DERIVE' ,
      'A' : 'ABSOLUTE' }
   # FIXME : Continuation lines (???) not implemented
   line = lines.pop(0)
   # FIXME No purging done of final spaces
   while 1 :
      # /^([^\s\[]+)?\[([^\s\/]+)\](\s+(.*))?$/
      #              ^^^^^^^^^^^^^^
      # We require starting with '[', and match chars different from ']' or spaces
      # /^([^\s\[]+)?\[([^\s\/]+)\](\s+(.*))?$/
      #   ^^^^^^^^^^
      # Optionally, a leading block of characters is allowed (except spaces and '[', which is reserver)
      # /^([^\s\[]+)?\[([^\s\/]+)\](\s+(.*))?$/
      #                            ^^^^^^^^^
      # The remaining text is also captured just to clean leading whitespaces
      """The perl module should be checked for config
BaseStep 300
[atemp]
frate[temp] My temperature !!!
G 2 -273 5000
[dtemp]
doto[ctemp] Miranda
That should give
*DS:atemp:COUNTER:900:U:U
DS:temp:GAUGE:600:-273:5000
*DS:dtemp:COUNTER:900:U:U
*DS:ctemp:COUNTER:900:U:U
"""
      match = re.match("([^\s\[]+)?\[([^\s\/]+)\](\s+(.*))?$", line[:-1] )
      if match :
         label = match.groups()[1]
         type , beat , min , max = "COUNTER", 3 , "U", "U"
         if not lines :
            DSs.append( "*DS:%s:%s:%s:%s:%s" % ( label , type , beat * step , min , max ) )
            break
         else :
            line = lines.pop(0)
            # FIXME No purging done of final spaces
            match = re.match("((C|G|D|A)\s?)?\s*([0-9\.]+)?(\s+((U|-?[0-9\.]+)\s+)?(U|-?[0-9\.]+))?$", line )
            if match :
               if match.groups()[1] : type = types[ match.groups()[1] ]
               if match.groups()[2] : beat = int(match.groups()[2])
               if match.groups()[5] : min = match.groups()[5]
               if match.groups()[6] : max = match.groups()[6]
               DSs.append( "DS:%s:%s:%s:%s:%s" % ( label , type , beat * step , min , max ) )
               if not lines : break
               line = lines.pop(0)
               # FIXME No purging done of final spaces
            else :
               DSs.append( "*DS:%s:%s:%s:%s:%s" % ( label , type , beat * step , min , max ) )
      else :
         if not lines : break
         line = lines.pop(0)
         # FIXME No purging done of final spaces

   return DSs

def getRRAs ( conf ) :
   TimeStep = 1
   RRAs = []
   tagname , filelines = getFileContents( conf )
   if tagname is False :
      return []
   for line in filelines :
      match = re.match("TimeStep\s+([0-9]+)",line)
      if match :
         TimeStep *= int(match.groups()[0])
         continue
      match = re.match("(CF\s+)?(AVERAGE|MAX|MIN|LAST)", line )
      if match :
         CF = match.groups()[1]
         xff , step , rows = 0.5 , 1 , 192
         match2 = re.match("(\s+(0|0?\.[0-9]+)?\s*([1-9][0-9]*)?)?\s+([0-9]+)$", line[match.end():] )
         if match2 :
            if match2.groups()[1] : xff = match2.groups()[1]
            if match2.groups()[2] : step = int(match2.groups()[2])
            if match2.groups()[3] : rows = match2.groups()[3]
         RRAs.append( "RRA:%s:%s:%s:%s" % ( CF , xff , step * TimeStep , rows ) )
         continue
   if not RRAs : RRAs = [ "RRA:AVERAGE:0.5:1:2048" ]
   return RRAs


"""
sub getIntervals($) {
   my $conf = shift;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( /^I\s+([^\s]+)\s+(\d+)/ ) {
         $graphInterval{$1} = "-$2";
         }
      }
   close CFG;
   }
"""

"""
sub getSize($) {
   my $size = shift;
   $size =~ /^(.*)x(.*)/;
   my $xsize = $1 || $defGraph[0];
   my $ysize = $2 || $defGraph[1];
   return "--width" , "$xsize" , "--height" , "$ysize";
   }
"""

def getGraphs ( conf ) :
   opts = ""
   graphs = {}
   tagname , filelines = getFileContents( conf )
   if tagname is False :
      return []
   for line in filelines :
      if re.match("\s*#",line) : continue
      # FIXME : No endline space cleaning, no emptyline detection
      match = re.match("O(=?)\s+",line)
      if match :
         if match.groups()[0] : opts = ""
         opts += line[match.end():-1]
         continue
      # /(P(S|H)?)(_?([a-z_]+))?(\s+([0-9]*x[0-9]*))?\s+(STD|[a-z,]+)\s+/
      #  ^^^^^^^^^
      # Starting with a P, optionally followed by a S or H, to define the plot type
      # /(P(S|H)?)(_?([a-z_]+))?(\s+([0-9]*x[0-9]*))?\s+(STD|[a-z,]+)\s+/
      #           ^^^^^^^^^^^^^
      # Optionally follows a text bock, maybe started with a '_' which defines a symbolic name for the plot
      # /(P(S|H)?)(_?([a-z_]+))?(\s+([0-9]*x[0-9]*))?\s+(STD|[a-z,]+)\s+/
      #                         ^^^^^^^^^^^^^^^^^^^^^
      # And optionally followed for a graph for the size
      # /(P(S|H)?)(_?([a-z_]+))?(\s+([0-9]*x[0-9]*))?\s+(STD|[a-z,]+)\s+/
      #                                                 ^^^^^^^^^^^^^
      # Then comes the mandatory list of intervals to be plotted, that might be replaced with 'STD' for default intervals
      match = re.match("(P(S|H)?)(_?([a-z_]+))?(\s+([0-9]*x[0-9]*))?\s+(STD|[a-z,]+)\s+", line )
      if match :
         vars = line[match.end():-1].split()
         size = []
         if match.groups()[5] : size = match.groups()[5]
         idx = ".".join( vars )
         if match.groups()[3] : idx = match.groups()[3]
         graphs[idx] = {}
         graphs[idx]['type'] = match.groups()[0]
         # NOTE : On perl, we store here a reference to the list
         graphs[idx]['size'] = size
         period = match.groups()[6]
         if period == "STD" : period = "daily,weekly,monthly,quarterly,yearly"
         # FIXME : Why this strange check ???
         if graphs[idx].has_key('periods') :
            graphs[idx]['periods'] += "," + period
         else :
            graphs[idx]['periods'] = period
         graphs[idx]['opts'] = opts
         # NOTE : On perl, we store here a reference to the list
         graphs[idx]['ds'] = vars
   return graphs


"""
sub getLabels($) {
   my $conf = shift;
   my %labels;
   return unless open CFG , "$confdir/$conf";
   while (<CFG>) {
      next if /^\s*#/; s/\s*$//;
      if ( /^([^\s\[]*)?\[([^\s\/]+)\](\s+(.*))?$/ ) {
         $labels{$2} = $4 || $1 || $2;
         $labels{$2} =~ s/\s*$//;
         }
      }
   close CFG;
   return %labels;
   }
"""

def rrdCreate ( host , target ) :
   rrdname = getNAME['rrd']( host , target )
   if os.path.isfile( rrdname ) :
      return "%s for %s exists, not overwritten" % ( target , host )
   _datadir = os.path.join( datadir , host )
   if not os.path.isdir( _datadir ) :
      os.mkdir( _datadir )
   _graphdir = os.path.join( graphdir , host , target )
   if not os.path.isdir( _graphdir ) :
      os.makedirs( _graphdir )
   try :
      cmdargs = [ "--step" , str( getStep( target ) ) ]
      cmdargs.extend( getDSs( target ) )
      cmdargs.extend( getRRAs( target ) )
      rrdtool.create( rrdname , *cmdargs )
   except rrdtool.error, ex :
      return "%s" % ex

"""
# To use also with PH plots, the title and other items differ
sub rrdGraph {
   my ( $host , $target , $graph , $period , @graphargs ) = @_ ;
   my $imgname = $getNAME{'img'}( $host , $target , $graph , $period );
   RRDs::graph ( $imgname , "--title=$target @ $host" , @graphargs );
   return RRDs::error;
   }
"""

# FIXME: What about using step for refresh time
refresh = "<META HTTP-EQUIV=Refresh CONTENT=3600>"
style = "bgcolor=#ffffff text=#000000"
tabstyle = "BORDER=0 CELLPADDING=0 CELLSPACING=10"

"""
sub openWeb($$) {
   my ( $file , $title ) = @_ ;
   open my $html , '>' , "$graphdir/$file.html" ;
   print $html "<HTML>\n<HEAD>\n";
   print $html "$refresh\n";
   print $html "<TITLE>$title</TITLE>\n";
   print $html "</HEAD>\n<BODY $style>\n";
   print $html "<TABLE $tabstyle>\n";
   return $html;
   }
"""

"""
sub closeWeb($) {
   my $html = shift ;
   print $html "</TABLE>\n";
   print $html "</BODY>\n</HTML>\n";
   close $html;
}
"""

"""
sub htmlAnchor($@) {
   my $src = shift ;
   # Thist should be a call to getNAME{'html'}
   my $html = join "/" , @_ ;
   return "<tr><td>" .
      ( $html ? "<A HREF=$html.html>" : "" ) .
      "<IMG SRC=$src>" .
      ( $html ? "</A>" : "" ) .
      "</td></tr>\n";
   }
"""

CFs = { 'a' : 'AVERAGE' , 'm' : 'MIN' , 'M' : 'MAX' , 'l' : 'LAST' }

"""
sub cfType($) {
   my $dsname = shift;
   my $type = 'AVERAGE';
#WinXP   return ( $dsname , $type );
   $dsname =~ s/^(a|m|M|l)_//;
   $type = $CFs{$1} if $1;
   return ( $dsname , $type );
   }
"""

"""
$getDEFs{'PH'} = sub {
   my ( $item , $target , $legend , @hosts ) = @_;
   my ( $n , $cf , @cmd );
   ( $item , $cf ) = cfType $item;
   for $n ( 0 .. $#hosts ) {
      if ( ref($hosts[$n]) eq "ARRAY" ) {
         $hosts[$n] = $hosts[$n]->[0];
         }
      my $rrdname = $getNAME{'rrd'}( $hosts[$n] , $target );
      $rrdname =~ s/:/\\:/;
      push @cmd , "DEF:d$n=$rrdname:$item:$cf";
      push @cmd , "LINE1:d$n#$colors[$n]:$hosts[$n]";
      }
   return @cmd;
   };
$getDEFs{'PS'} = sub {
   my ( $host , $target , $legend , @items ) = @_;
   my ( $item , $cf , @cmd );
   ( $item , $cf ) = cfType $items[0];
   my $rrdname = $getNAME{'rrd'}( $host , $target );
   push @cmd , "DEF:d0=$rrdname:$item:$cf";
   push @cmd , "AREA:d0#$colors[0]:$$legend{$item}";
   my $n;
   for $n ( 1 .. $#items ) {
      my ( $item , $cf ) = cfType $items[$n];
      my $rrdname = $getNAME{'rrd'}( $host , $target );
      $rrdname =~ s/:/\\:/;
      push @cmd , "DEF:d$n=$rrdname:$item:$cf";
      push @cmd , "STACK:d$n#$colors[$n]:$$legend{$item}";
      }
   return @cmd;
   };
$getDEFs{'P'} = sub {
   my ( $host , $target , $legend , @items ) = @_;
   my ( $item , $cf );
   my ( $n , @cmd ) = ( 0 , () );
   for $n ( 0 .. $#items ) {
      ( $item , $cf ) = cfType $items[$n];
      my $rrdname = $getNAME{'rrd'}( $host , $target );
      $rrdname =~ s/:/\\:/;
      push @cmd , "DEF:d$n=$rrdname:$item:$cf";
      push @cmd , "LINE1:d$n#$colors[$n]:$$legend{$item}";
      }
   return @cmd;
   };
"""

# FIXME: no index yet
# IMPROVE: The easiest way is just use a single char as separator, betwen
#          host & target , $graph & period , ....
getNAME = {}
getNAME['rrd'] = lambda h,t : os.path.join( datadir , h , "%s.rrd"%t )
getNAME['img'] = lambda h,t,g,p : os.path.join ( graphdir , h,t,g, "%s.png"%p )
"""
$getNAME{'src'} = sub {
   my $period = pop ;
   return join( "/" , @_ ) . ".$period.png";
   };
sub htmName($$;$) {
   my $host = shift;
   my $target = shift;
   if ( @_ ) { $target .= "/" . shift }
   if ( $host eq "-" ) { return $target }
   return "$graphdir/$host/$target.html";
   }
"""

if __name__ == "__main__" :

   rootdir = "examples"
   confdir = "examples"

   if os.sys.argv[1:] :
       confs = os.sys.argv[1:]
   else :
       confs = [  "temperature" , "cacheSysPerf" , "process" , "vmstat" ]

   for file in confs :
      print
      print "File",file
      print "----"
      print
      print "\n".join( getDSs( file ) )
      print
      print "\n".join( getRRAs( file ) )
      print
      graphs = getGraphs( file )
      for key in graphs.keys() :
         print key,":::",graphs[key]
      print
      hosts = getHosts( file )
      print "Hosts : %s" % " ".join(hosts)
      print
      for host in hosts :
         print getNAME['rrd']( host , file )
      print
      tags = getTags( file )
      print "Tags : %s" % " ".join( tags )
      for tag in tags :
         hosts = getHosts( "%s.%s" % ( file , tag ) )
         print "Tag : %s -->> %s" % ( tag , " ".join(hosts) )
   print

   if not os.sys.argv[1:] :
      print
      print "Creation"
      print
      datadir = os.path.join(rootdir,"data")
      if not os.path.isdir( datadir ) :
         os.mkdir( datadir )
      conf = "vmstat"
      #
      res = rrdCreate( "host2" , conf )
      if res :
         print "ERROR : %s" % res
      else :
         print "host2/%s created" % conf
         print
      try :
         os.unlink( os.path.join( datadir , "host2" , "%s.rrd" % conf ) )
      except Exception , ex :
         print ex
      try :
         os.rmdir( os.path.join( datadir , "host2" ) )
      except Exception , ex :
         print ex

