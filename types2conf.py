#!/usr/bin/python

import glob
import time
import os

collectd_dir = "/var/lib/collectd/rrd/"

file_names = []
if os.path.isfile( "/etc/collectd/collectd.conf" ) :
    fd = open( "/etc/collectd/collectd.conf" )
    for line in fd.readlines() :
        if line.find("TypesDB") != -1 :
            items = line[:-1].split()
            items.pop(0)
            file_names.extend( [ x.strip("'\"") for x in items ] )
if not file_names :
    file_names = [ "/usr/share/collectd/types.db" ]

graphs = {
    'cpu': [ 'C_global STD cpu-softirq cpu-system cpu-user cpu-interrupt cpu-wait cpu-steal cpu-nice',
             'CS_stack STD cpu-softirq cpu-system cpu-user cpu-interrupt cpu-wait cpu-steal cpu-nice cpu-idle' ],
    'processes': [ 'C_global STD ps_state-running ps_state-paging ps_state-stopped ps_state-zombies ps_state-blocked',
                   'C_sleeping STD ps_state-sleeping',
                   'C_forks STD fork_rate' ],
    'interface': [ 'C_global STD if_octets.rx if_octets.tx',
                   'C_errror STD if_errors.rx if_errors.tx',
                   'C_packets STD if_packets.rx if_packets.tx'],
    'postgresql': [ 'C_global STD pg_xact-commit pg_xact-rollback',
                    'C STD pg_blks-heap_hit pg_blks-heap_read pg_blks-idx_hit pg_blks-idx_read pg_blks-tidx_hit pg_blks-tidx_read pg_blks-toast_hit pg_blks-toast_read',
                    'C STD pg_db_size',
                    'C STD pg_n_tup_c-del pg_n_tup_c-hot_upd pg_n_tup_c-ins pg_n_tup_c-upd pg_n_tup_g-dead pg_n_tup_g-live ',
                    'C STD pg_numbackends',
                    'C STD pg_scan-idx pg_scan-idx_tup_fetch pg_scan-seq pg_scan-seq_tup_read ']
    }

print

# Store all metric_types known to collectd
metrics = {}
for fd in map( open , file_names ) :

    line = fd.readline()
    while line :
        items = line.strip().split()
        if items :
            metric = items.pop(0)
            if not metric.startswith('#') :
                ds_string = map( lambda x : x.strip(',') , items )
                metrics[metric] = map( lambda x : tuple(x.split(':')) , ds_string )
        line = fd.readline()


confs = {}
instances = {}
plugin2metric = {}

now = time.time()

for rrd in glob.glob( "%s*/*/*.rrd" % collectd_dir ) :

  stat = os.stat(rrd)
  if now - stat.st_atime < 10 * 3600 :

    host , plugin , metric_type = os.path.relpath( rrd , collectd_dir ).split('/')
    plugin = plugin.split('-',1)
    plugin.append('')
    metric_type = os.path.splitext(metric_type)[0].split('-')
    metric_type.append('')

    metric = metric_type[0]
    instance = metric_type[0]

    if not plugin[0] == metric_type[0] :
        metric = plugin[0]
        if not plugin2metric.has_key(metric) :
            plugin2metric[metric] = { instance:{} }
        elif not plugin2metric[metric].has_key(instance) :
            plugin2metric[metric][instance] = {}
        plugin2metric[metric][instance][metric_type[1]] = True
        instance = plugin[1]

    elif not metrics.has_key(metric) :
        continue

    if not instances.has_key(metric) :
        instances[metric] = { instance:{} }
    elif not instances[metric].has_key( instance ) :
        instances[metric][ instance ] = {}
    instances[metric][instance][metric_type[1]] = True

    if not confs.has_key(metric) :
        confs[metric] = { host:{} }
    elif not confs[metric].has_key( host ) :
        confs[metric][ host ] = {}
    confs[metric][host][plugin[1]] = True

for conf in confs :
    for key in confs[conf].keys() :
        confs[conf][key] = confs[conf][key].keys()

for instance in instances :
    for key in instances[instance].keys() :
        instances[instance][key] = instances[instance][key].keys()

for metric in plugin2metric :
    for key in plugin2metric[metric].keys() :
        plugin2metric[metric][key] = plugin2metric[metric][key].keys()


for k in confs :

    print "Writing %s" % k
    print
    fd = open( "RRDUTILS_ROOT/conf/%s" % k , 'w' )
    fd.write( "\n" )

    hosts = {}
    for host in confs[k] :
      for tag in confs[k][host] :
        if not hosts.has_key( tag ) :
            hosts[tag] = []
        hosts[tag].append( host )

    tags = hosts.keys()
    tags.sort()
    for tag in tags :
        fd.write( "H%s %s\n" % ( tag , " ".join(hosts[tag]) ) )
    fd.write( "\n" )

    datasources = []
    if plugin2metric.has_key(k) :
        for metric in plugin2metric[k] :
            for datasource in metrics[metric] :
                for instance in plugin2metric[k][metric] :
                    metric_name = metric
                    if instance :
                        metric_name += "-%s" % instance
                    metric_name += ".%s" % datasource[0]
                    datasources.append( metric_name )
                    out = "[%s]" % metric_name
                    out += "\n%.1s %s %s" % datasource[1:]
                    fd.write( "%s\n" % out )
    elif metrics.has_key(k) :
      for metric_type in instances[k] :
        for metric in metrics[k] :
            for instance in instances[k][metric_type] :
                metric_name = metric_type
                if instance :
                    metric_name += "-%s" % instance
                metric_name += ".%s" % metric[0]
                datasources.append( metric_name )
                out = "[%s]" % metric_name
                out += "\n%.1s %s %s" % metric[1:]
                fd.write( "%s\n" % out )
    fd.write( "\n" )

    fd.write( "DefGraph global\n" )
    if graphs.has_key( k ) :
        for graph in graphs[k] :
            fd.write( "%s\n" % graph )
    else :
        fd.write( "C_global STD %s\n" % " ".join( [ ds.replace('.value', '') for ds in datasources ] ) )
    fd.write( "\n" )

fd.write( "\n" )

