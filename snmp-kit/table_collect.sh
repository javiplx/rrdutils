#!/bin/sh

AWK=awk
[ `uname -s` == "SunOS" ] && AWK=nawk

PATH=$PATH:/usr/local/bin
export PATH

EXTRA_DIRS=""
if [ "$1" = "-M" ] ; then
   EXTRA_DIRS="-M$2"
   shift 2
   fi

EXTRA_MIBS=""
if [ "$1" = "-m" ] ; then
   EXTRA_MIBS="-m $2"
   shift 2
   fi

PORT=161
if [ "$1" = "-p" ] ; then
   PORT=$2
   shift 2
   fi

VERSION="2c"
if [ "$1" = "-v" ] ; then
   VERSION="$2"
   shift 2
   fi

COMMUNITY="public" 
if [ "$1" = "-c" ] ; then
   COMMUNITY="$2"
   shift 2
   fi

if [ $# -lt 2 -o $# -gt 3 ] ; then
   echo "Usage: table-collect.sh [-M extra-dirs] [-m extra-mibs] [-p port] [-v version] [-c comm] host oidTable [tblIndex]"
   exit
   fi

MIBS="$EXTRA_DIRS $EXTRA_MIBS"
MIBS="-Ob -Ou $MIBS"

HOST=$1
TABLE=$2
IDX=${3:-.*}

# En esta forma, el sed no parece funcionar, y lo movemos al awk
# sed -n -e 's/\.'$IDX' = /&/p'
snmpwalk $MIBS -v $VERSION -c $COMMUNITY $HOST:$PORT $TABLE \
 | sed -e 's/^.*'$TABLE'\.[^.]*Entry.\([^.]*\)\.\(.*\) = /\1\|\2\|/' \
     -e 's/\(Counter\|Gauge\)[0-9]*: //' -e 's/ \(Packets\|Octets\|Collisions\)//' \
     -e 's/IpAddress: //' -e 's/Timeticks: ([0-9]*) //' -e 's/INTEGER: //' \
     -e 's/OID: //' -e 's/Hex-STRING: //' -e 's/STRING: //' \
 | $AWK -F \| '
BEGIN {
#write["ifIndex"]=1
# ...
}
$2 ~ /^'$IDX'$/ {
colname[$1] = 1
indx[$2] = 1
value[$1,$2] = $3
}
END {
printf "%s","Index"
for ( c in colname ) printf "\t%s",c
#for ( c in write ) printf "|%s",c
printf "\n"

for ( i in indx ) {
   printf "%s|", i
   for ( c in colname ) printf "|%s",value[c,i]
#   for ( c in write ) printf "|%s",value[c,i]
   printf "\n"
   }
}'

