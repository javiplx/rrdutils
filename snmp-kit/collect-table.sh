#!/bin/sh

RRDDIR="/var/lib/rrd"

PATH=$PATH:/usr/local/bin
export PATH

EXTRA_DIRS=""
if [ "$1" = "-M" ] ; then
   EXTRA_DIRS="-M$2"
   shift 2
   fi

EXTRA_MIBS=""
if [ "$1" = "-m" ] ; then
   EXTRA_MIBS="-m $2"
   shift 2
   fi

PORT=161
if [ "$1" = "-p" ] ; then
   PORT=$2
   shift 2
   fi

VERSION="2c"
if [ "$1" = "-v" ] ; then
   VERSION="$2"
   shift 2
   fi

COMMUNITY="public"
if [ "$1" = "-c" ] ; then
   COMMUNITY="$2"
   shift 2
   fi

if [ $# -eq 0 ] ; then
   echo "Usage: collect-table.sh [-M extra-dirs] [-m extra-mibs] [-p port] [-v version] [-c comm] table [host1 host2 ...]"
   exit
   fi

MIBS="$EXTRA_DIRS $EXTRA_MIBS"
MIBS="-Ob -Ou -Ov $MIBS"

conf=$1
shift

conff="$RRDDIR/conf/$conf"
HOSTS=${*:-`sed -n 's/^T  *\([^ ][^ ]*\)  *.*$/\1/p' $conff`}
OIDS=`sed -n -e 's/^\[\(.*\)\].*$/\1/p'  -e 's/^\([^\[#].*\)\[.*$/\1/p' $conff`
DATADIR=`sed -n 's/DataDir  *//p' $conff`
DATADIR=${DATADIR:-"$RRDDIR/data"}


for host in $HOSTS ; do
   for idx in `sed -n 's/^T  *'$host'  *//p' $conff` ; do
      data=""
      DATE=`date +%s`
# Solaris has no %s option, although is quite the same in any case
DATE="N"
      for oid in $OIDS ; do
         result=`snmpget $MIBS -v $VERSION -c $COMMUNITY $host:$PORT $oid.$idx | cut -d\  -f2`
         data="$data:$result"
         done
       rrdtool update $DATADIR/$host/$conf.$idx.rrd $DATE$data
      done
   done
