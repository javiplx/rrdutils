#!/bin/sh

AWK=awk
[ `uname -s` == "SunOS" ] && AWK=nawk

EXTRA_DIRS=""
if [ "$1" = "-M" ] ; then
   EXTRA_DIRS="-M$2"
   shift 2
   fi

EXTRA_MIBS=""
if [ "$1" = "-m" ] ; then
   EXTRA_MIBS="-m $2"
   shift 2
   fi

PORT=161
if [ "$1" = "-p" ] ; then
   PORT=$2
   shift 2
   fi

VERSION="2c"
if [ "$1" = "-v" ] ; then
   VERSION="$2"
   shift 2
   fi

COMMUNITY="public"
if [ "$1" = "-c" ] ; then
   COMMUNITY="$2"
   shift 2
   fi

if [ $# -gt 2 -o $# -eq 0 ] ; then
   echo "Usage: crea_conf.sh [-M extra-dirs] [-m extra-mibs] [-p port] [-v version] [-c comm] host [oid]"
   exit
   fi

MIBS="$EXTRA_DIRS $EXTRA_MIBS"
MIBS="-Ob -Ou $MIBS"

HOST=$1
MIB=$2

rm -rf tmp/$HOST
mkdir -p tmp/$HOST

#
# configurations for 'normal' OID
#

cat <<EOF > /tmp/base.$$

BaseStep 600

AVERAGE 288

TimeStep 6
AVERAGE 1480
MIN 1480
MAX 1480

TimeStep 24
AVERAGE 730
MIN 730
MAX 730

AVERAGE 7 312
MIN 7 312
MAX 7 312

EOF

#echo snmpwalk $MIBS -c $COMMUNITY $HOST:$PORT $MIB
# The second line for solaris must be
#     -e 's/^\(.*\)\.\([^.]*\)\.0 = \(\([^:]*\):* *\)/\1 \2 \4_/p' \
# so we lose the \5 parsing part
snmpwalk $MIBS -v $VERSION -c $COMMUNITY $HOST:$PORT $MIB | sed -n \
     -e 's/^\([^=]*= \)\([^:]*\): \([-A-Za-z]*([0-9][0-9]*)\) */\1\2_\3 /p' \
     -e 's/^\([^=]*= \)\([^:]*\): /\1\2_ /p' | tee /tmp/snmpout.$$ \
  | sed -n -e '/Table\./ d' \
     -e 's/^\(.*\)\.\([^.]*\)\.0 = /\1 \2 /p' \
  | while read file oid comment ; do
      [ -f tmp/$HOST/$file ] || cp /tmp/base.$$ tmp/$HOST/$file
      echo "[$oid] $oid $comment" >> tmp/$HOST/$file
      done

# This was when using linux
#  | sed -n -e '/Table\./ d' \
#      -e 's/^\(.*\)\.\([^.]*\)\.0 = \(\([^:]*\): \)\?\([A-Za-z][-A-Za-z]*([0-9][0-9]*)\)\?.*$/\1 \2 \4_\5/p' \
#  | while read file oid comment ; do
#      [ -f tmp/$HOST/$file ] || cp /tmp/base.$$ tmp/$HOST/$file
#      echo "[$oid] $oid $comment" >> tmp/$HOST/$file
#      done

##
## Configuration for tables
##

# The same problem here, where
#   -n 's/^\(.*\)\.\([^\.]*Table\)\.[^\.]*Entry\.\([^\.]*\)\.\([^ ]*\) = \(\([^:]*\):* *\)/\1 \2 \3 \4 \6_/p
#
# This was when using linux
#sed -n 's/^\(.*\)\.\([^.]*Table\)\.[^.]*Entry\.\([^.]*\)\.\([^ ]*\) = \(\([^:]*\): \)\?\([A-Za-z][-A-Za-z]*([0-9][0-9]*)\)\?.*$/\1 \2 \3 \4 \6_\7/p' /tmp/snmpout.$$ \
cat /tmp/snmpout.$$ | sed -n \
-e 's/^\(.*\)\.\([^.]*Table\)\.[^.]*Entry\.\([^.]*\)\.\([^ ]*\) = /\1 \2 \3 \4 /p' \
  | $AWK '
function printIdx() {
   indexes=""
   for ( i in idx ) { indexes=sprintf("%s %s",indexes,i) ; delete idx[i] }
   if ( indexes != "" ) { printf "T '$HOST'%s\n\n" , indexes >> outchann }
# Este tampoco funciona en el nawk de solaris
#   delete idx
   }
file[$1,$2] != 1 {
   printIdx()
   command=sprintf("cp /tmp/base.'$$' tmp/'$HOST'/%s.%s",$1,$2)
   system(command)
   outchann=sprintf("tmp/'$HOST'/%s.%s",$1,$2)
   file[$1,$2] = 1
   }
ds[$3] != 1 {
   printf "[%s] %s" , $3 , $3 >> outchann
   for ( i=5 ; i<=NF ; i++) {
      printf " %s" , $i >> outchann
      }
   printf "\n" >> outchann
   ds[$3] = 1
   }
{ idx[$4] = 1 }
END { printIdx() }
'

rm /tmp/snmpout.$$ /tmp/base.$$


