#!/usr/bin/perl
# By Henry Steinhauer

use warnings;
use strict;
use RRDs;

BEGIN { push @INC , "."; }

use RRDutils qw( :DEFAULT :Graph );

die "\nUsage RRDcreate conf [host1 host2 ...]\n\n" unless @ARGV;

my $target = shift;
my $now_string;
my ( $conf , $tag ) = split /\./ , $target ;
#die "No '$conf' configuration file found\n" unless -f "$confdir/$conf";

my @hosts = @ARGV;
@hosts=getHosts $target unless @hosts;
die "No host to process\n" unless @hosts;

changeDefs $conf;

getIntervals $conf;

my %graphs = getGraphs $conf;
die "No graph defined" unless %graphs;

my %legend = getLabels $conf;


my %CFs;
$CFs{'a'} = 'AVERAGE';
$CFs{'m'} = 'MIN';
$CFs{'M'} = 'MAX';
$CFs{'l'} = 'LAST';

sub cfType($) {
   my $dsname = shift;
   my $type = 'AVERAGE';
   $dsname =~ s/^(a|m|M|l)_//;
   $type = $CFs{$1} if $1;
   return ( $dsname , $type );
   }

my ( %getDEFs , %getPRINTs );
$getDEFs{'PH'} = sub {
   my ( $item , $target , @hosts ) = @_;
   my ( $n , $cf , $cmd );
   ( $item , $cf ) = cfType $item ;
   for $n ( 0 .. $#hosts ) {
      if ( ref($hosts[$n]) eq "ARRAY" ) {
         $hosts[$n] = $hosts[$n]->[0];
         }
      $cmd .= " DEF:d$n=$datadir/$hosts[$n]/$target.rrd:$item:$cf";
      $cmd .= " LINE2:d$n#$colors[$n]:$hosts[$n]";
      }
   return $cmd;
   };
$getDEFs{'PS'} = sub {
   my ( $host , $target , @items ) = @_;
   my ( $item , $cf , $cmd );
   ( $item , $cf ) = cfType $items[0] ;
   $cmd  = " DEF:d0=$datadir/$host/$target.rrd:$item:$cf";
   $cmd .= " AREA:d0#$colors[0]:\"$legend{$item}\"";
   my $n;
   for $n ( 1 .. $#items ) {
      my ( $item , $cf ) = cfType $items[$n] ;
      $cmd .= " DEF:d$n=$datadir/$host/$target.rrd:$item:$cf";
      $cmd .= " STACK:d$n#$colors[$n]:\"$legend{$item}\"";
      }
   return $cmd;
   };
$getDEFs{'P'} = sub {
   my ( $host , $target , @items ) = @_;
   my ( $item , $cf );
   my ( $n , $cmd ) = ( 0 , "" );
   for $n ( 0 .. $#items ) {
      ( $item , $cf ) = cfType $items[$n] ;
#     $cmd .= " DEF:d$n=$datadir/$host/$target.rrd:$item:$cf";
      $cmd .= " DEF:d$n=$host.rrd:$item:$cf";
      $cmd .= " LINE2:d$n#$colors[$n]:\"$legend{$item}\"";
      }
   return $cmd;
   };
$getPRINTs{'P'} = sub {
   my ( $host , $target , @items ) = @_;
   my ( $item , $cf );
#   my ( $n , $cmd ) = ( 0 , ' COMMENT:"\\s" COMMENT:"\\s"' );
   my ( $n , $cmd ) = ( 0 , '');
   for $n ( 0 .. $#items ) {
      ( $item , $cf ) = cfType $items[$n] ;
      $cmd .= " DEF:p$n=$datadir/$host/$target.rrd:$item:$cf";
      $cmd .= " GPRINT:p$n:$cf:\'$item   $cf   %.1lf%S\\r\'";
      }
   return $cmd;
   };
##########


# The unless is the trick for PH plots with SNMP tables
$target =~ s+^.*\.++ unless ref($hosts[0]) eq "ARRAY" ;

##  --
#   Create the top level Index page Index1
#      one entry per Host - Use first Graph
my ( $host , $graph );
Index1: for $graph ( sort keys %graphs) {
   my $period = $graphs{$graph}{'period'};
  if ($period eq "daily") {
   open (HTML,">$graphdir/$conf-i.html");
    my $d1 = qq(<HTML><HEAD> <TITLE> BMC Index for $conf </TITLE></HEAD>);
  
   print HTML "----------   Start of  Index1  -----------\n  ";
   print HTML "$d1 \n";
     $d1 = qq(<BODY bgcolor="#ffffff" text="#000000"><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=10>);
   print HTML "$d1 \n";
   
    my $cmdargs = "$graph.gif";
   for $host ( @hosts ) {
      my $cmd="$graphdir/$host-$cmdargs";
    my $double1 = qq(<TR><TD><DIV><A HREF="$conf-$host.html">);
    my $double2 = qq(<IMG BORDER=1 ALT="$host $graph" SRC="$cmd"></A><BR>);
    my $double3 = qq(<SMALL><!--#flastmod file="$conf-$host.html" --></SMALL></DIV></TD></TR>);
      print HTML "$double1 $double2 $double3 \n";
     }
   last Index1
    }
     
   }
my     $d1 = qq(</TABLE></BODY></HTML>);
   print HTML "$d1 \n";
   close (HTML);
my $ab = 1 ;

##  --
#   Create the top level Index page Index2
#      one page per Host -
#        one entry per graph
#        refers to history of each item.
Index2: for $host ( @hosts ) {


open (HTML,">$graphdir/$conf-$host.html");
    my $d1 = qq(<HTML><HEAD> <TITLE> BMC Index for $conf $host </TITLE></HEAD>);
   print HTML "----------   Start of  Index1  $host -----------\n  ";
   print HTML "$d1 \n";
     $d1 = qq(<BODY bgcolor="#ffffff" text="#000000"><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=10>);
   print HTML "$d1 \n";

        for $graph ( sort keys %graphs) {
        
   my $period = $graphs{$graph}{'period'};
  if ($period eq "daily") {
    my $cmdargs = "$graph.gif";
    my $cmd="$graphdir/$host-$cmdargs";
    my $double1 = qq(<TR><TD><DIV><A HREF="$conf-$host-$graph.html">);
    my $double2 = qq(<IMG BORDER=1 ALT="$host-$graph" SRC="$cmd"></A><BR>);
    my $double3 = qq(<SMALL><!--#flastmod file="$conf-$host-$graph.html" --></SMALL></DIV></TD></TR>);
      print HTML "$double1 $double2 $double3 \n";
     }
    }
        $d1 = qq(</TABLE></BODY></HTML>);
   print HTML "$d1 \n";
   close (HTML);
   }


 $ab = 1;

##  --
#   Create the top level Index page Index2
#      one page per Host -
#        one entry per graph
#        refers to history of each item.
Index3: for $host ( @hosts ) {

        for $graph ( sort keys %graphs) {

    my $period = $graphs{$graph}{'period'};
   if ($period eq "daily") {
open (HTML,">$graphdir/$conf-$host-$graph.html");
    my $d1 = qq(<HTML><HEAD> <TITLE> BMC Index for $conf $host </TITLE></HEAD>);
   print HTML "----------   Start of  Index1  $host $graph -----------\n  ";
   print HTML "$d1 \n";
     $d1 = qq(<BODY bgcolor="#ffffff" text="#000000"><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=10>);
   print HTML "$d1 \n";
    };
    my $cmdargs = "$graph.gif";
    my $cmd="$graphdir/$host-$cmdargs";
    my $double1 = qq(<TR><TD><DIV>);
    my $double2 = qq(<IMG BORDER=1 ALT="$conf-$host-$graph" SRC="$cmd"></A><BR>);
    my $double3 = qq(</DIV></TD></TR>);
      print HTML "$double1 $double2 $double3 \n";
     }
       my     $d1 = qq(</TABLE></BODY></HTML>);
   print HTML "$d1 \n";
   close (HTML);
     }
 $ab = 1;

##



