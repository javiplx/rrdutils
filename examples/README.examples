
1. Basic example

As a practical example we will construct a configuration file for the
example on rrdcreate man page. The command line used there is

<manpage>
     rrdtool create temperature.rrd --step 300
     DS:temp:GAUGE:600:-273:5000 RRA:AVERAGE:0.5:1:1200
     RRA:MIN:0.5:12:2400 RRA:MAX:0.5:12:2400
     RRA:AVERAGE:0.5:12:2400
<manpage>

and we will construct for it the configuration file 'temperature'

<manpage>
     This sets up an RRD called temperature.rrd which accepts one
     temperature value every 300 seconds. If no new data is
     supplied for more than 600 seconds, the temperature becomes
     *UNKNOWN*.  The minimum acceptable value is -273 and the
     maximum is 5000.
<manpage>

So the base interval for the whole RRD is 300 seconds. Regarding the data
sources, there is only one, called temp, with a heartbeat of 600 seconds.
In the conf file the heartbeat is expressed in terms of the base interval,
so the 600 becomes 600/300 = 2. The minimun and maximum goes directly

<temperature>
BaseStep 300

[temp]
G 2 -273 5000
<temperature>

<manpage>
     A few archives areas are also defined. The first stores the
     temperatures supplied for 100 hours (1200 * 300 seconds =
     100 hours). The second RRA stores the minimum temperature
     recorded over every hour (12 * 300 seconds = 1 hour), for
     100 days (2400 hours). The third and the fourth RRA's do the
     same the maximum and average temperature, respectively.
<manpage>

The consolidation functions are also straightforward, with the first being

<temperature>
CF AVERAGE 0.5 1 1200
<temperature>

Now we will use the 'TimeStep' tag, intended to make easier the reading of
the configuration file. With the factor 12 we move the reference interval
from 300 seconds to 1 hour (300*12=3600). With this change, the consolidation
interval of the next consolidation functions is written as 1 instead of
12 as it was on the command line. The usefulness of the 'TimeStep' factor
becomes clearer if we add later another consolidation function for the
whole day, that we write as 24 (hours) instead of using the not so easy
288 (24 * 12 * 300 seconds = 1 day). The reference interval could be
later expanded again with a factor 24 to use '7' to refer to a whole week
consolidation interval.
As we can drop the "0.5 1", which are default values, the coming RRAs are
writeen as

<temperature>
TimeStep 12

CF MIN 2400
CF MAX 2400
CF AVERAGE 2400
<temperature>

1.1 rrdUtils extensions

So far is not evident the need (even the help) that rrdUtils have, appart
from the inclusion of items to create weekly, monthly or yearly graps, with
lines like 'P monthly temperature'. But, let us assume that we have not only
one termometer, but many of them: one outside, and four inside, one for every
room. We add the line
H outside room1 room2 room3 room4
and we can create the 5 rrd files on one shot with `RRDcreate temperature`.
And construct the graphs for all of them with `RRDgrapher temperature`. If
we get an extra termometer for first room, we can issue `RRDcreate temperature
room1_bis` and we get its rrd file ready. And, after including in a Host line
on the configuration file, we could graph it with its brothers with the same
`RRDgrapher temperature` used before (and thus no changes in any graphing
script).

In case we get a mixture of Celsius and Farenheit termometers, we can call
to the tagged configurations. For this example, we must throw up any limits
impossed to the datasource, but the idea is to be illustrative instead of
precise. With lines as
Hcelsius inside room1_bis room2
Hfarenheit room1 room3 room4
in the configuration files, we can use the commands with temperature.celsius
and temperature.farenheit in the same way that we previously used temperature,
and being applied only to their respective termometers.


2. Other examples

The examples.dir directory on the distribution also includes two example
configuration files, one to store the values of the cacheSysPerf SNMP tree
from squid, and another one taken from our in-house monitoring system to
store the results from linux `vmstat` command. The SNMP example can be used
with the snmp-kit tools, which are in a usable but preliminar state.

An extra configuration file is included to show tagged configurations, which
deals with some aspect of running ftpd, httpd and mysqld processes for a
certain number of hosts.

