#!/usr/bin/python

from xml.dom.minidom import parse

import sys

ds_name = sys.argv[1]

doc = parse( sys.stdin )
root = doc.documentElement

ds_position = None
ds_match = None

count = 0
for xml in root.childNodes :
    if xml.nodeName == 'ds' :
        count += 1
        ds = xml.getElementsByTagName('name')[0].firstChild.nodeValue.strip()
        if ds == ds_name :
            ds_position = count
            ds_match = xml
    if xml.nodeName == 'rra' :
        cdp_prep = xml.getElementsByTagName('cdp_prep')
        pos = 0
        for ds in xml.getElementsByTagName('cdp_prep')[0].childNodes :
            if ds.nodeName == 'ds' :
                pos += 1
                if pos == ds_position :
                    quiet = cdp_prep[0].removeChild(ds)
        for entry in xml.getElementsByTagName('database')[0].childNodes :
            if entry.nodeName == 'row' :
                vpos = 0
                for data in entry.childNodes :
                    if data.nodeName == 'v' :
                        vpos += 1
                        if vpos == ds_position :
                            quiet = entry.removeChild(data)

quiet = root.removeChild(ds_match)

doc.writexml(sys.stdout)

